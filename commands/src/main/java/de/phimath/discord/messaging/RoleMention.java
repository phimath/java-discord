/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: RoleMention
 * Filename: RoleMention.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.messaging;

import org.javacord.api.DiscordApi;
import org.javacord.api.entity.permission.Role;

import java.util.regex.Pattern;

public class RoleMention {
    public static final String PATTERN = "(?>\\\\\\\\)*<@&(?<id>[0-9]++)>";
    public static final Pattern COMPILED_PATTERN = Pattern.compile(PATTERN);

    public final long targetId;

    public RoleMention(String input) {
        var match = COMPILED_PATTERN.matcher(input);
        if (match.matches()) {
            targetId = Long.parseLong(match.group("id"));
        } else {
            throw new IllegalStateException("Cannot find role id in string " + input);
        }
    }

    public Role resolve(DiscordApi api) {
        return api.getRoleById(targetId).orElse(null);
    }

    public static Role resolve(Object o, DiscordApi api) {
        return ((RoleMention) o).resolve(api);
    }
}
