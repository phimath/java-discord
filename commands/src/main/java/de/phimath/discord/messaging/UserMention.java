/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: UserMention
 * Filename: UserMention.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.messaging;

import org.javacord.api.DiscordApi;
import org.javacord.api.entity.user.User;

import java.util.regex.Pattern;

public class UserMention {
    public static final String PATTERN = "(?>\\\\\\\\)*<@!?+(?<id>[0-9]++)>";
    public static final Pattern COMPILED_PATTERN = Pattern.compile(PATTERN);

    public final long targetId;

    public UserMention(String input) {
        var match = COMPILED_PATTERN.matcher(input);
        if (match.matches()) {
            targetId = Long.parseLong(match.group("id"));
        } else {
            throw new IllegalStateException("Cannot find user id in string " + input);
        }
    }

    public User resolve(DiscordApi api) {
        return api.getUserById(targetId).join();
    }

    public static User resolve(Object o, DiscordApi api) {
        return ((UserMention) o).resolve(api);
    }
}
