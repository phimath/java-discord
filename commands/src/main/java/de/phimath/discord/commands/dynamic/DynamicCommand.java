/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: DynamicCommand
 * Filename: DynamicCommand.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.commands.dynamic;

import de.phimath.discord.commands.*;
import de.phimath.discord.messaging.MessageHandler;

import java.lang.annotation.Annotation;

/**
 * Implement this interface to specify a dynamic command, which means you have control over its internals at runtime.
 * <p>
 * This usage opposes static commands, which are specified via the {@link Command} annotation.
 */
public interface DynamicCommand extends Command, MessageHandler {
    String value();

    default String prefix() {
        return "!";
    }

    default boolean acceptContent() {
        return false;
    }

    default Parameter[] parameters() {
        return new DynamicParameter[0];
    }

    default Roles allowedRoles() {
        return new DynamicRoles(new long[0]);
    }

    default Roles blockedRoles() {
        return new DynamicRoles(new long[0]);
    }

    default long[] allowedUsers() {
        return new long[0];
    }

    default long[] blockedUsers() {
        return new long[0];
    }

    default long[] alloweChannels() {
        return new long[0];
    }

    default long[] blockedChannels() {
        return new long[0];
    }

    default Class<? extends Annotation> annotationType() {
        return Command.class;
    }

    /**
     * Override this method to define a custom "correct usage" message for this command.
     * <p>
     * Otherwise, the command defaults to {@link CommandManager.Artifact#defaultUsage(Command)};
     */
    default String usage() {
        return CommandManager.defaultUsage(this);
    }
}
