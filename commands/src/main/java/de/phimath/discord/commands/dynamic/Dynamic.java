/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: Dynamic
 * Filename: Dynamic.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.commands.dynamic;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate your dynamic commands with {@link Dynamic}.
 * <p>
 * The dynamic command should implement the {@link DynamicCommand} interface, which implicates implementing the {@link de.phimath.discord.messaging.MessageHandler} interface.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Dynamic {
}
