/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: DynamicRoles
 * Filename: DynamicRoles.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.commands.dynamic;

import de.phimath.discord.commands.Roles;

import java.lang.annotation.Annotation;

/**
 * Reference implementation which is used for specifying {@link Roles} in the environment of a {@link DynamicCommand}.
 */
public class DynamicRoles implements Roles {
    private long[] roles;
    private boolean allowEmpty = true;

    public DynamicRoles(long[] roles) {
        this.roles = roles;
    }

    public DynamicRoles(long[] roles, boolean allowEmpty) {
        this.roles = roles;
        this.allowEmpty = allowEmpty;
    }

    public long[] value() {
        return roles;
    }

    public boolean allowEmpty() {
        return allowEmpty;
    }

    public Class<? extends Annotation> annotationType() {
        return Roles.class;
    }
}
