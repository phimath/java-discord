/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: Command
 * Filename: Command.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.commands;

import org.javacord.api.entity.message.Message;

import java.lang.annotation.*;

/**
 * Annotate your static commands with {@link Command}.
 * <p>
 * The static command should implement the {@link de.phimath.discord.messaging.MessageHandler} interface.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(Commands.class)
public @interface Command {
    /**
     * The command name to recognize.
     */
    String value();

    /**
     * The prefix for the command name {@link #value()}, which can be used to differentiate the command when used in combination with other (common) bots.
     * <p>
     * By default, the prefix is "!".
     */
    String prefix() default "!";

    /**
     * Setting this value to {@code true} allows the user to enter an arbitrary {@link String} at the end of the command (after all {@link #parameters()}),
     * which is then handed over to the target {@link de.phimath.discord.messaging.MessageHandler#onMessage(Message, String, Object...)} as the second parameter.
     * <p>
     * By default, does not allow the variable length {@link String} at the end.
     */
    boolean acceptContent() default false;

    /**
     * The {@link Parameter} array which are accepted after the {@link #prefix()} and {@link #value()}, but before the (optional) {@link #acceptContent()}.
     */
    Parameter[] parameters() default {};

    /**
     * The {@link Roles} object specifying which Role IDs are allowed for this command. Yet, {@link #blockedRoles()} and {@link #blockedUsers()} override this value.
     * <p>
     * By default, allows everyone.
     */
    Roles allowedRoles() default @Roles({});

    /**
     * The {@link Roles} object specifying which Role IDs are disallowed for this command. Yet, {@link #allowedUsers()} overrides this value.
     * <p>
     * By default, blocks no one.
     */
    Roles blockedRoles() default @Roles({});

    /**
     * The User ID array specifying which Users are explicitly allowed for this command. Yet, {@link #blockedUsers()} overrides this value.
     * <p>
     * By default, unset.
     */
    long[] allowedUsers() default {};

    /**
     * The User ID array specifying which Users are explicitly disallowed for this command. This value cannot be overridden.
     * <p>
     * By default, unset.
     */
    long[] blockedUsers() default {};

    /**
     * The Channel ID array specifying which Channels are explicitly allowed for this command. Yet, {@link #blockedChannels()} overrides this value.
     * <p>
     * By default, unset.
     */
    long[] alloweChannels() default {};

    /**
     * The Channel ID array specifying which Channels are explicitly disallowed for this command. This value cannot be overridden.
     * <p>
     * By default, unset.
     */
    long[] blockedChannels() default {};
}
