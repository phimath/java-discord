/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: CommandManager
 * Filename: CommandManager.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.commands;

import de.phimath.discord.commands.dynamic.Dynamic;
import de.phimath.discord.commands.dynamic.DynamicCommand;
import de.phimath.discord.messaging.ChannelMention;
import de.phimath.discord.messaging.MessageHandler;
import de.phimath.discord.messaging.RoleMention;
import de.phimath.discord.messaging.UserMention;
import de.phimath.discord.processing.CommandProcessor;
import de.phimath.discord.shards.ShardManager;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.DiscordEntity;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CommandManager {
    /**
     * This class represents a command with its message handler, recognizer and the usage string.
     */
    public class Artifact {
        /**
         * The underlying static {@link Command} or dynamic {@link DynamicCommand}.
         */
        final Command command;
        /**
         *
         */
        final Supplier<Pattern> pattern;
        final MessageHandler handler;
        final Supplier<String> usage;

        /**
         * Constructor for dynamic commands
         *
         * @param command The dynamic command
         */
        Artifact(DynamicCommand command) {
            this.command = command;
            this.handler = command;
            this.pattern = () -> compileCommand(this.command);
            this.usage = command::usage;
        }

        /**
         * Constructor for static commands
         *
         * @param command The static command
         * @param pattern The pre-compile pattern for the command
         * @param handler The associated handler for this command
         */
        Artifact(Command command, Pattern pattern, MessageHandler handler) {
            this.command = command;
            this.pattern = () -> pattern;
            this.handler = handler;
            this.usage = () -> defaultUsage(command);
        }
    }

    public static class MessageInfo {
        public final Optional<User> user;
        public final Optional<Server> server;
        public final Set<Long> roles;
        public final Message message;

        MessageInfo(Message message) {
            this.message = message;
            this.user = message.getUserAuthor();
            this.server = message.getServer();
            this.roles = user.map(u -> server.map(s -> u.getRoles(s).stream().map(DiscordEntity::getId).collect(Collectors.toUnmodifiableSet()))
                    .orElse(Collections.emptySet())).orElse(Collections.emptySet());
        }
    }

    /**
     * Default usage generator for the given command.
     *
     * @param command The input command
     * @return The default usage for the given command
     */
    public static String defaultUsage(Command command) {
        return "Correct usage: " +
                command.prefix() +
                command.value() + " " +
                Arrays.stream(command.parameters()).map(p -> {
                    var clazz = p.value();
                    if (clazz.isEnum()) {
                        return "{" + clazz.getSimpleName() + ": " + Arrays.stream(clazz.getEnumConstants()).map(Object::toString).collect(Collectors.joining(", ")) + "}";
                    } else {
                        return "{" + clazz.getSimpleName() + "}";
                    }
                }).collect(Collectors.joining(" ")) +
                (command.acceptContent() ? " <variable content>" : "");
    }

    /**
     * The artifacts known by the current runtime.
     */
    private List<Artifact> artifacts = new ArrayList<>();

    /**
     * The map which contains the known classes and their parser functions.
     */
    private Map<Class<?>, Function<String, ?>> parsers = new HashMap<>();

    /**
     * The thread-safe map which contains message infos mapped by the hash code of the {@link MessageHandler} object.
     */
    private Map<Integer, MessageInfo> messageInfos = new ConcurrentHashMap<>();

    /**
     * @param handler The handler which requests the current message information.
     * @return The message info which is currently handled.
     * @see MessageInfo
     * @see MessageHandler
     * @see Object#hashCode()
     */
    public MessageInfo getMessageInfo(MessageHandler handler) {
        return messageInfos.get(handler.hashCode());
    }

    /**
     * Construct a CommandManager for the given {@link DiscordApi} and register the local handler {@link #notify(Message)}.
     *
     * @param manager The shard manager for the bot
     */
    public CommandManager(ShardManager manager) {
        try {
            Class<?> nameClass = Class.forName(CommandProcessor.OUT_FULL_NAME);
            // -- PARSERS --
            //noinspection unchecked
            this.parsers = (Map<Class<?>, Function<String, ?>>) nameClass.getField(CommandProcessor.OUT_NAME_PARSERS).get(null);
            // -- STATIC COMMANDS --
            var nameField = nameClass.getField(CommandProcessor.OUT_NAME_COMMANDS.toUpperCase());
            @SuppressWarnings("unchecked")
            var commandNames = (List<Class<? extends MessageHandler>>) nameField.get(null);
            for (var command : commandNames) {
                register(command);
            }
            // -- DYNAMIC COMMANDS --
            var dynamicNameField = nameClass.getField(CommandProcessor.OUT_NAME_DYNAMIC);
            @SuppressWarnings("unchecked")
            var dynamicCommandNames = (List<Class<? extends DynamicCommand>>) dynamicNameField.get(null);
            for (var command : dynamicCommandNames) {
                register(command);
            }
        } catch (ClassNotFoundException e) {
            System.err.println("Cannot find compiled annotation class. Make sure that your project is " +
                    "compiled with " + CommandProcessor.class.getCanonicalName() + " as the processor.");
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Cannot find the field \"" + CommandProcessor.OUT_NAME_COMMANDS.toUpperCase() + "\" or \"" + CommandProcessor.OUT_NAME_DYNAMIC + "\".");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // Register the handler
        manager.addOnConnected(api -> api.addMessageCreateListener(e -> notify(e.getMessage())));
    }

    /**
     * Register the given class type as a message handler and compile it to an {@link Artifact}.
     *
     * @param clazz
     * @return An instance of the input class type.
     */
    private void register(Class<? extends MessageHandler> clazz) {
        try {
            var ctor = clazz.getConstructors()[0];
            if (ctor.getParameterCount() != 0) {
                throw new RuntimeException("Message handler class " + clazz.getSimpleName() + " must contain a no-args constructor.");
            }

            MessageHandler handler = (MessageHandler) ctor.newInstance();

            boolean isDynamic = clazz.getAnnotationsByType(Dynamic.class).length > 0;

            if (isDynamic) {
                var command = (DynamicCommand) handler;
                artifacts.add(new Artifact(command));
            } else {
                for (Command command : clazz.getAnnotationsByType(Command.class)) {
                    artifacts.add(new Artifact(command, compileCommand(command), handler));
                }
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Compile a command to a regex recognizer.
     *
     * @param command
     * @return
     */
    private Pattern compileCommand(Command command) {
        StringBuilder pattern = new StringBuilder();
        pattern.append("^[").append(command.prefix().replace("^", "\\^").replace("\\", "\\\\")).append("]");
        pattern.append(command.value());
        var params = command.parameters();
        for (int i = 0; i < params.length; i++) {
            pattern.append("(?<param").append(i).append(">(?> (?>").append(parameterToPattern(params[i])).append("))").append(params[i].repeat()).append(")");
        }
        if (command.acceptContent()) {
            pattern.append(" (?<content>.*)");
        }
        pattern.append("$");
        return Pattern.compile(pattern.toString());
    }

    /**
     * Generate a partial pattern to use in {@link #compileCommand(Command), depending on the given {@link Parameter}.
     *
     * @param parameter
     * @return
     */
    private String parameterToPattern(Parameter parameter) {
        var clazz = parameter.value();

        String limiter;
        {
            int limit = parameter.characterLimit();
            if (limit == 0) {
                throw new IllegalArgumentException("Cannot expect parameter of length 0!");
            } else if (limit > 0) {
                limiter = "{1, " + limit + "}";
            } else {
                limiter = "+";
            }
        }
        return classToPattern(clazz, limiter);
    }

    /**
     * For the given class, look for all generic interfaces it implements with the lookFor parameter, then return the list of unique type arguments.
     *
     * @param genericClass
     * @param lookFor
     * @return
     */
    private static List<Class<?>> extractParameters(Class<?> genericClass, Class<?> lookFor) {
        if (lookFor.getTypeParameters().length != 1) {
            throw new RuntimeException("Doesnt work on super classes/interfaces with more than 1 generic parameter");
        }
        return (lookFor.isInterface() ? Arrays.stream(genericClass.getGenericInterfaces()) : Stream.of(genericClass.getGenericSuperclass()))
                .map(t -> (ParameterizedType) t)
                .filter(t -> lookFor.isAssignableFrom((Class<?>) t.getRawType()))
                .map(t -> Arrays.asList(t.getActualTypeArguments()))
                .flatMap(List::stream)
                .map(t -> (Class<?>) t)
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * Generate a partial pattern based on the input class and the given limiter.
     *
     * @param clazz
     * @param limiter
     * @return
     */
    private String classToPattern(Class<?> clazz, String limiter) {
        String pattern;
        if (clazz == byte.class || clazz == short.class || clazz == int.class || clazz == long.class) {
            pattern = "\\d" + limiter;
        } else if (clazz == ChannelMention.class) {
            pattern = ChannelMention.PATTERN;
        } else if (clazz == UserMention.class) {
            pattern = UserMention.PATTERN;
        } else if (clazz == RoleMention.class) {
            pattern = RoleMention.PATTERN;
        } else if (clazz == String.class) {
            pattern = "[^\\r\\n\\t\\f\\v \"]" + limiter + "|(?>\"." + limiter + "\")";
        } else if (parsers.containsKey(clazz)) {
            pattern = "\\{(.*?)\\}";
        } else if (Collection.class.isAssignableFrom(clazz)) {
            var innerClasses = extractParameters(clazz, Collection.class);
            var innerPattern = classToPattern(innerClasses.get(0), limiter);  // TODO: is this okay to assume taking the first element?
            pattern = "\\[\\s*\\]|\\[" + innerPattern + "(?>\\s*,\\s*" + innerPattern + ")*\\]";
        } else {
            pattern = "\\S" + limiter;
        }

        return pattern;
    }

    /**
     * For the given input String, convert it to the outclass type.
     *
     * @param input    The part of the
     * @param outclass
     * @return
     */
    private Object cast(String input, Class<?> outclass) {
        if (outclass == String.class) {
            return input;
        } else if (outclass == boolean.class || outclass == Boolean.class) {
            return Boolean.parseBoolean(input);
        } else if (outclass == byte.class || outclass == Byte.class) {
            return Byte.parseByte(input);
        } else if (outclass == short.class || outclass == Short.class) {
            return Short.parseShort(input);
        } else if (outclass == int.class || outclass == Integer.class) {
            return Integer.parseInt(input);
        } else if (outclass == long.class || outclass == Long.class) {
            return Long.parseLong(input);
        } else if (outclass == Color.class) {
            try {
                return Color.class.getField(input.toUpperCase()).get(null);
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
                e.printStackTrace();
                return null;
            }
        } else if (outclass == ChannelMention.class) {
            return new ChannelMention(input);
        } else if (outclass == RoleMention.class) {
            return new RoleMention(input);
        } else if (outclass == UserMention.class) {
            return new UserMention(input);
        } else if (outclass.isEnum()) {
            //noinspection unchecked
            return Enum.valueOf((Class<? extends Enum>) outclass, input.toUpperCase());
        } else if (parsers.containsKey(outclass)) {
            return parsers.get(outclass).apply(input.substring(1, input.length() - 1));
        } else if (Collection.class.isAssignableFrom(outclass)) {
            if (Pattern.matches("\\[\\s*\\]", input)) {
                return Collections.emptyList();
            }
            var innerClasses = extractParameters(outclass, Collection.class);  // TODO: as above: okay if only one?
            var innerClass = innerClasses.get(0);
            var innerPattern = Pattern.compile(classToPattern(innerClass, ""));
            var matcher = innerPattern.matcher(input);
            List<Object> out = new ArrayList<>();
            while (matcher.find()) {
                out.add(cast(matcher.group(0), innerClass));
            }
            return out;
        }
        throw new RuntimeException("Unsupported target class " + outclass.getSimpleName());
    }

    private void notify(Message message) {
        var info = new MessageInfo(message);
        artifacts.forEach(a -> {
            var handler = a.handler;
            var hashCode = handler.hashCode();
            messageInfos.put(hashCode, info);
            var command = a.command;
            var pattern = a.pattern.get();
            var m = pattern.matcher(message.getContent());
            if (message.getContent().startsWith(command.prefix() + command.value() + (command.acceptContent() || command.parameters().length > 0 ? " " : ""))) {
                if (m.matches()) {
                    var channelId = message.getChannel().getId();
                    //noinspection PointlessBooleanExpression
                    if (true &&
                            // If channel is blocked, no point in continuing
                            (command.blockedChannels().length > 0 && Arrays.stream(command.blockedChannels()).noneMatch(blockedChannel -> blockedChannel == channelId)) &&
                            (command.alloweChannels().length > 0 && Arrays.stream(command.alloweChannels()).anyMatch(allowedChannel -> allowedChannel == channelId)) &&
                            // If user is blocked, do not allow it no matter what
                            message.getUserAuthor().map(ua -> Arrays.stream(command.blockedUsers()).noneMatch(u -> ua.getId() == u)).orElse(true) &&
                            (false ||
                                    // Either allow it by user
                                    message.getUserAuthor().map(ua -> Arrays.stream(command.allowedUsers()).anyMatch(u -> ua.getId() == u)).orElse(false) ||
                                    // Or from the roles
                                    (true &&
                                            ((command.allowedRoles().value().length > 0) ?
                                                    (!info.roles.isEmpty() && Arrays.stream(command.allowedRoles().value()).anyMatch(info.roles::contains)) :
                                                    command.allowedRoles().allowEmpty()) &&

                                            ((command.blockedRoles().value().length > 0) ?
                                                    (!info.roles.isEmpty() && Arrays.stream(command.blockedRoles().value()).noneMatch(info.roles::contains)) :
                                                    command.blockedRoles().allowEmpty()
                                            )
                                    )
                            )
                    ) {
                        var content = command.acceptContent() ? m.group("content").trim() : "";
                        var params = command.parameters();
                        Object[] outParams = new Object[params.length];
                        for (int i = 0; i < params.length; i++) {
                            outParams[i] = cast(m.group("param" + i).substring(1), params[i].value());
                        }
                        handler.onMessage(message, content, outParams);
                    } else {
                        message.getUserAuthor().ifPresent(u -> u.sendMessage("Not permitted to use command " + command.value()));
                    }
                } else {
                    message.getChannel().sendMessage(a.usage.get());
                }
            }
            messageInfos.remove(hashCode);
        });
    }
}
