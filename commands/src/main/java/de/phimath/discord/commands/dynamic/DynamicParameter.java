/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: DynamicParameter
 * Filename: DynamicParameter.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.commands.dynamic;

import de.phimath.discord.commands.Parameter;

import java.lang.annotation.Annotation;

/**
 * Reference implementation which is used for specifying {@link Parameter} in the environment of a {@link DynamicCommand}.
 */
public class DynamicParameter implements Parameter {
    /**
     * @see Parameter#value()
     */
    private Class<?> resultClass;
    /**
     * @see Parameter#characterLimit()
     */
    private int characterLimit = -1;
    /**
     * @see Parameter#repeat()
     */
    private String regexRepeat = "";

    /**
     * @see Parameter#value()
     */
    public DynamicParameter(Class<?> resultClass) {
        this.resultClass = resultClass;
    }

    /**
     * @see Parameter#value()
     * @see Parameter#characterLimit()
     */
    public DynamicParameter(Class<?> resultClass, int characterLimit) {
        this.resultClass = resultClass;
        this.characterLimit = characterLimit;
    }

    /**
     * @see Parameter#value()
     * @see Parameter#repeat()
     */
    public DynamicParameter(Class<?> resultClass, String regexRepeat) {
        this.resultClass = resultClass;
        this.regexRepeat = regexRepeat;
    }

    /**
     * @see Parameter#value()
     * @see Parameter#characterLimit()
     * @see Parameter#repeat()
     */
    public DynamicParameter(Class<?> resultClass, int characterLimit, String regexRepeat) {
        this.resultClass = resultClass;
        this.characterLimit = characterLimit;
        this.regexRepeat = regexRepeat;
    }

    public Class<?> value() {
        return resultClass;
    }

    public int characterLimit() {
        return characterLimit;
    }

    public String repeat() {
        return regexRepeat;
    }

    public Class<? extends Annotation> annotationType() {
        return Parameter.class;
    }
}
