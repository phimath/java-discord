/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: Parameter
 * Filename: Parameter.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.commands;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Parameter {
    /**
     * The Java {@link Class} which is expected.
     */
    Class<?> value();

    /**
     * The regex repeat pattern as {@link String}, which is none by default (1 occurrence).
     */
    String repeat() default "";

    /**
     * The character limit for this parameter, or {@code -1} otherwise.
     */
    int characterLimit() default -1;
}
