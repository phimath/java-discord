/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: CommandProcessor
 * Filename: CommandProcessor.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.processing;

import de.phimath.discord.commands.Command;
import de.phimath.discord.commands.Commands;
import de.phimath.discord.commands.Parser;
import de.phimath.discord.commands.dynamic.Dynamic;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;

public class CommandProcessor extends AbstractProcessor {
    public static final String OUT_PACKAGE = "de.phimath.discord.commands";
    public static final String OUT_NAME_COMMANDS = "CommandNames";
    public static final String OUT_NAME_DYNAMIC = OUT_NAME_COMMANDS.toUpperCase() + "_DYNAMIC";
    public static final String OUT_NAME_PARSERS = "PARSERS";
    public static final String OUT_FULL_NAME = OUT_PACKAGE + "." + OUT_NAME_COMMANDS;

    // Types
    private final Set<String> supportedAnnotations = Set.of(
            Command.class.getCanonicalName(),
            Commands.class.getCanonicalName(),
            Dynamic.class.getCanonicalName(),
            Parser.class.getCanonicalName()
    );

    private final Set<String> classNames = new HashSet<>();
    private final Set<String> dynClassNames = new HashSet<>();
    private final Set<String> parsers = new HashSet<>();

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        if (set.isEmpty()) {
            return true;
        }

        for (Element element : roundEnvironment.getElementsAnnotatedWith(Command.class)) {
            classNames.add(element.asType().toString());
        }
        for (Element element : roundEnvironment.getElementsAnnotatedWith(Commands.class)) {
            classNames.add(element.asType().toString());
        }
        for (Element element : roundEnvironment.getElementsAnnotatedWith(Dynamic.class)) {
            dynClassNames.add(element.asType().toString());
        }
        for (Element element : roundEnvironment.getElementsAnnotatedWith(Parser.class)) {
            var parentClass = element.getEnclosingElement();
            var asMethod = (ExecutableElement) element;
            var parameters = asMethod.getParameters();
            if (parameters.size() != 1) {
                this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "Parser method can only have exactly one argument of type String");
            }
            if (!parameters.get(0).asType().toString().equals(String.class.getCanonicalName())) {
                this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "Parser method input parameter must be of type String");
            }
            if (!asMethod.getReturnType().toString().equals(parentClass.toString())) {
                this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "Return type of parser method must be of the same type as its parent class");
            }
            if (!element.getModifiers().contains(Modifier.STATIC)) {
                this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "Parser method must be static!");
            }
            parsers.add(parentClass.asType().toString() + "::" + asMethod.getSimpleName().toString());
        }
        {
            JavaFileObject jfo;
            try {
                jfo = processingEnv.getFiler().createSourceFile(OUT_FULL_NAME);

                Writer writer = jfo.openWriter();
                writer.write("package " + OUT_PACKAGE + ";\n");
                writer.write("import javax.lang.model.element.*;\n");
                writer.write("import java.util.*;\n");
                writer.write("import java.util.function.*;\n");
                writer.write("import de.phimath.discord.messaging.*;\n");
                writer.write("import de.phimath.discord.commands.dynamic.*;\n");
                writer.write("public abstract class " + OUT_NAME_COMMANDS + " {\n");
                var varname = OUT_NAME_COMMANDS.toUpperCase();
                writer.write("public final static List<Class<? extends MessageHandler>> " + varname + " = new ArrayList<>();\n");
                writer.write("public final static List<Class<? extends DynamicCommand>> " + OUT_NAME_DYNAMIC + " = new ArrayList<>();\n");
                writer.write("public final static Map<Class<?>, Function<String, ?>> " + OUT_NAME_PARSERS + " = new HashMap<>();\n");
                writer.write("static {\n");
                for (var className : classNames) {
                    writer.write(varname + ".add(" + className + ".class);\n");
                }
                for (var className : dynClassNames) {
                    writer.write(OUT_NAME_DYNAMIC + ".add(" + className + ".class);\n");
                }
                for (var parser : parsers) {
                    writer.write(OUT_NAME_PARSERS + ".put(" + parser.split("::")[0] + ".class, " + parser + ");\n");
                }
                writer.write("}\n");
                writer.write("}\n");
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return true;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return supportedAnnotations;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
