/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: ShardManager
 * Filename: ShardManager.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.shards;

import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.server.Server;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

public class ShardManager {
    private final int shardCount;
    private final String token;
    private final boolean trustAllCerts;
    private boolean initialized = false;

    public ShardManager(int shardCount, String token) {
        this(shardCount, token, false);
    }

    public ShardManager(int shardCount, String token, boolean trustAllCerts) {
        this.shardCount = shardCount;
        this.token = token;
        this.trustAllCerts = trustAllCerts;
    }

    private Map<Integer, DiscordApi> shards = new ConcurrentHashMap<>();
    private Collection<Consumer<DiscordApi>> onConnected = new LinkedList<>();
    private Collection<Consumer<DiscordApi>> onDisconnected = new LinkedList<>();

    public void addOnConnected(Consumer<DiscordApi> handler) {
        onConnected.add(handler);
    }

    public void addOnDisconnected(Consumer<DiscordApi> handler) {
        onDisconnected.add(handler);
    }

    private void onConnected(DiscordApi api) {
        onConnected.forEach(c -> c.accept(api));
    }

    private void onDisconnected(DiscordApi api) {
        onDisconnected.forEach(c -> c.accept(api));
    }

    public Collection<DiscordApi> allShards() {
        return Collections.unmodifiableCollection(shards.values());
    }

    public DiscordApi get(int shardId) {
        if (shardId < 0 || shardId >= this.shardCount) {
            throw new IllegalArgumentException("Cannot get shard with id " + shardId + ", when there are only " + this.shardCount + " shards");
        }
        return this.shards.get(shardId);
    }

    public DiscordApi get(long serverId) {
        return get((int) (serverId >> 22) % this.shardCount);
    }

    public DiscordApi get(Server server) {
        return get(server.getId());
    }

    public void teardown() {
        this.shards.values().forEach(DiscordApi::disconnect);
    }

    public void init() {
        if (initialized) {
            throw new IllegalStateException();
        }
        new DiscordApiBuilder()
                .setTrustAllCertificates(trustAllCerts)
                .setToken(token)
                .setTotalShards(shardCount)
                .loginAllShards()
                .stream()
                .map(CompletableFuture::join)
                .forEach(shard -> {
                    onConnected(shard);
                    this.shards.put(shard.getCurrentShard(), shard);
                });

        try {
            Thread.sleep(1500);
        } catch (InterruptedException ignored) {
        }
        initialized = true;
    }

    public void reinit(int shardId) {
        if (shardId < 0 || shardId >= this.shardCount) {
            throw new IllegalArgumentException("Cannot reconnect shard with id " + shardId + ", when there are only " + this.shardCount + " shards");
        }
        var newShard = new DiscordApiBuilder()
                .setTrustAllCertificates(trustAllCerts)
                .setToken(token)
                .setTotalShards(shardCount)
                .setCurrentShard(shardId)
                .login()
                .join();
        onConnected(newShard);

        var oldShard = this.shards.put(shardId, newShard);
        onDisconnected(oldShard);
        oldShard.disconnect();

        try {
            Thread.sleep(1500);
        } catch (InterruptedException ignored) {
        }
    }
}
