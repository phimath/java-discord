/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: PluginManager
 * Filename: PluginManager.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.plugins;

import de.phimath.discord.processing.PluginProcessor;
import de.phimath.discord.shards.ShardManager;
import org.javacord.api.entity.message.Message;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.event.message.reaction.ReactionAddEvent;
import org.javacord.api.event.server.member.ServerMemberBanEvent;
import org.javacord.api.event.server.member.ServerMemberJoinEvent;
import org.javacord.api.event.server.member.ServerMemberLeaveEvent;
import org.javacord.api.event.server.member.ServerMemberUnbanEvent;
import org.javacord.api.event.server.role.UserRoleAddEvent;
import org.javacord.api.event.server.role.UserRoleRemoveEvent;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class PluginManager {
    public static class Artifact {
        public final Plugin annotation;
        public final DiscordPlugin plugin;

        public Artifact(Plugin annotation, DiscordPlugin plugin) {
            this.annotation = annotation;
            this.plugin = plugin;
        }
    }

    private List<Class<? extends DiscordPlugin>> classes = new ArrayList<>();
    private List<Artifact> artifacts = new ArrayList<>();

    public PluginManager(ShardManager manager) {
        this(manager, false);
    }

    public PluginManager(ShardManager manager, boolean processMessageOnEdit) {
        try {
            Class<?> nameClass = Class.forName(PluginProcessor.OUT_FULL_NAME);
            var nameField = nameClass.getField(PluginProcessor.OUT_NAME.toUpperCase());
            @SuppressWarnings("unchecked")
            List<String> commandNames = (List<String>) nameField.get(null);
            for (var command : commandNames) {
                try {
                    var clazz = (Class<? extends DiscordPlugin>) Class.forName(command);
                    classes.add(clazz);
                    register(clazz);
                } catch (ClassNotFoundException e) {
                    System.err.println("Cannot find annotated plugin class " + command + " which was present at compile time.");
                }
            }
            System.out.println();
        } catch (ClassNotFoundException e) {
            System.err.println("Cannot find compiled annotation class. Make sure that your project is " +
                    "compiled with " + PluginProcessor.class.getCanonicalName() + " as the processor.");
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Cannot find the field \"" + PluginProcessor.OUT_NAME.toUpperCase() + "\".");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        manager.addOnConnected(api -> api.addMessageCreateListener(event -> this.onMessage(event.getMessage())));
        if (processMessageOnEdit) {
            manager.addOnConnected(api -> api.addMessageEditListener(event -> event.getMessage().ifPresent(this::onMessage)));
        }
        manager.addOnConnected(api -> api.addUserRoleAddListener(this::onRoleGranted));
        manager.addOnConnected(api -> api.addUserRoleRemoveListener(this::onRoleRemoved));
        manager.addOnConnected(api -> api.addReactionAddListener(this::onReactionAdded));
        manager.addOnConnected(api -> api.addServerMemberJoinListener(this::onUserJoin));
        manager.addOnConnected(api -> api.addServerMemberLeaveListener(this::onUserLeave));
        manager.addOnConnected(api -> api.addServerMemberBanListener(this::onUserBanned));
        manager.addOnConnected(api -> api.addServerMemberUnbanListener(this::onUserUnbanned));
    }


    private DiscordPlugin register(Class<? extends DiscordPlugin> pluginClass) {
        try {
            var ctor = pluginClass.getConstructors()[0];
            if (ctor.getParameterCount() != 0) {
                throw new RuntimeException("Plugin target class " + pluginClass.getSimpleName() + " must contain a no-args constructor.");
            }
            DiscordPlugin discordPlugin = (DiscordPlugin) ctor.newInstance();
            Plugin plugin = pluginClass.getAnnotation(Plugin.class);
            artifacts.add(new Artifact(plugin, discordPlugin));
            return discordPlugin;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private void onMessage(Message message) {
        artifacts.forEach(a -> {
            var listenTo = a.annotation.listenTo();
            if ((listenTo == Plugin.ListenType.BOTH) ||
                    (listenTo == Plugin.ListenType.SERVER && message.isServerMessage()) ||
                    (listenTo == Plugin.ListenType.PRIVATE && message.isPrivateMessage())) {
                a.plugin.onMessage(message, message.getContent());
            }
        });
    }

    private void onRoleGranted(UserRoleAddEvent event) {
        artifacts.forEach(a -> a.plugin.onRoleGranted(event));
    }

    private void onRoleRemoved(UserRoleRemoveEvent event) {
        artifacts.forEach(a -> a.plugin.onRoleRemoved(event));
    }

    private void onReactionAdded(ReactionAddEvent event) {
        artifacts.forEach(a -> a.plugin.onReactionAdded(event));
    }

    private void onUserJoin(ServerMemberJoinEvent event) {
        artifacts.forEach(a -> a.plugin.onUserJoin(event));
    }

    private void onUserLeave(ServerMemberLeaveEvent event) {
        artifacts.forEach(a -> a.plugin.onUserLeave(event));
    }

    private void onUserBanned(ServerMemberBanEvent event) {
        artifacts.forEach(a -> a.plugin.onUserBanned(event));
    }

    private void onUserUnbanned(ServerMemberUnbanEvent event) {
        artifacts.forEach(a -> a.plugin.onUserUnbanned(event));
    }
}
