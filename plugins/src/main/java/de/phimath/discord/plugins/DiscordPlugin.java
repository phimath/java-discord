/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: DiscordPlugin
 * Filename: DiscordPlugin.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.plugins;

import de.phimath.discord.messaging.MessageHandler;
import org.javacord.api.entity.message.Message;
import org.javacord.api.event.message.reaction.ReactionAddEvent;
import org.javacord.api.event.server.member.ServerMemberBanEvent;
import org.javacord.api.event.server.member.ServerMemberJoinEvent;
import org.javacord.api.event.server.member.ServerMemberLeaveEvent;
import org.javacord.api.event.server.member.ServerMemberUnbanEvent;
import org.javacord.api.event.server.role.UserRoleAddEvent;
import org.javacord.api.event.server.role.UserRoleRemoveEvent;

public interface DiscordPlugin extends MessageHandler {
    @Override
    default void onMessage(Message message, String content, Object... parameters) {
    }

    default void onRoleGranted(UserRoleAddEvent event) {
    }

    default void onRoleRemoved(UserRoleRemoveEvent event) {
    }

    default void onReactionAdded(ReactionAddEvent event) {
    }

    default void onUserJoin(ServerMemberJoinEvent event) {
    }

    default void onUserLeave(ServerMemberLeaveEvent event) {
    }

    default void onUserBanned(ServerMemberBanEvent event) {
    }

    default void onUserUnbanned(ServerMemberUnbanEvent event) {
    }
}
