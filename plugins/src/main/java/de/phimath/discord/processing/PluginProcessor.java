/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: PluginProcessor
 * Filename: PluginProcessor.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.processing;

import de.phimath.discord.plugins.Plugin;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;

public class PluginProcessor extends AbstractProcessor {
    public static final String OUT_PACKAGE = "de.phimath.discord.plugins";
    public static final String OUT_NAME = "PluginNames";
    public static final String OUT_FULL_NAME = OUT_PACKAGE + "." + OUT_NAME;

    // Types
    private final Set<String> supportedAnnotations = Set.of(Plugin.class.getCanonicalName());

    private final Set<String> classNames = new HashSet<>();

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        if (set.isEmpty()) {
            return true;
        }

        classNames.clear();

        for (Element element : roundEnvironment.getElementsAnnotatedWith(Plugin.class)) {
            if (element.getModifiers().contains(Modifier.STATIC)) {
                processingEnv.getMessager().printMessage(
                        Diagnostic.Kind.ERROR,
                        String.format("Plugin class '%s' must not be static", element.getSimpleName().toString()),
                        element);
            }
            String elementType = element.asType().toString();

            classNames.add(elementType);
        }
        {
            JavaFileObject jfo;
            try {
                jfo = processingEnv.getFiler().createSourceFile(OUT_FULL_NAME);

                Writer writer = jfo.openWriter();
                writer.write("package " + OUT_PACKAGE + ";\n");
                writer.write("import javax.lang.model.element.*;\nimport java.util.*;\n");
                writer.write("public abstract class " + OUT_NAME + " {\n");
                var varname = OUT_NAME.toUpperCase();
                writer.write("public final static List<String> " + varname + " = new ArrayList<>();\n");
                writer.write("static {\n");
                for (var className : classNames) {
                    writer.write(varname + ".add(\"" + className + "\");\n");
                }
                writer.write("}\n");
                writer.write("}\n");
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return true;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return supportedAnnotations;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
