/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: PermissionImpl
 * Filename: PermissionImpl.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.permissions;

import javax.persistence.*;

@Entity
@Table(name = "permissions", schema = "main")
public class PermissionImpl {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue
    private long id;

    @Column(name = "GUILDID", nullable = false)
    private long guildId;

    @Column(name = "TARGETID", nullable = false)
    private long targetId;

    @Column(name = "TARGET", nullable = false)
    @Enumerated(EnumType.STRING)
    private PermissionTarget target;

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private PermissionType type;

    public PermissionImpl(long guildId, long targetId, PermissionTarget target) {
        this.guildId = guildId;
        this.targetId = targetId;
        this.target = target;
    }

    public PermissionImpl() {
    }

    public long getGuildId() {
        return guildId;
    }

    public void setGuildId(long guildId) {
        this.guildId = guildId;
    }

    public long getTargetId() {
        return targetId;
    }

    public void setTargetId(long targetId) {
        this.targetId = targetId;
    }

    public PermissionTarget getTarget() {
        return target;
    }

    public void setTarget(PermissionTarget target) {
        this.target = target;
    }
}
