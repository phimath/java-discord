/*
 * Discord Framework Java Library
 * Copyright (c)  2019  Philipp Matthäus
 * Licenced under GNU AGPLv3
 *
 * Class: Permission
 * Filename: Permission.java
 *
 * ===
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.phimath.discord.permissions;

import javax.persistence.EntityManager;

public class Permission {
    public static EntityManager EM = null;

    private boolean TRANSACTION = false;
    private PermissionImpl db;

    public Permission transaction() {
        if (EM == null) {
            throw new IllegalStateException("Cannot start transaction when Entity Manager is null");
        }
        if (TRANSACTION) {
            throw new IllegalStateException("Cannot start transaction when already active");
        }
        TRANSACTION = true;
        return this;
    }

    public Permission commit() {
        if (!TRANSACTION) {
            throw new IllegalStateException("Cannot commit transaction when not active");
        }
        if(EM.contains(db)) {
            db.setGuildId(this.guildId);
            db.setTargetId(this.targetId);
            db.setTarget(this.target);
            db = EM.merge(db);
        } else {
            db = new PermissionImpl(guildId, targetId, target);
            EM.persist(db);
        }
        TRANSACTION = false;
        return this;
    }

    private long guildId;
    private long targetId;
    private PermissionTarget target;
    private PermissionType type;

    public Permission(long guildId, long targetId, PermissionTarget target, PermissionType type) {
        this.guildId = guildId;
        this.targetId = targetId;
        this.target = target;
        this.type = type;
    }

    public long getGuildId() {
        return guildId;
    }

    public Permission setGuildId(long guildId) {
        this.guildId = guildId;
        return this;
    }

    public long getTargetId() {
        return targetId;
    }

    public Permission setTargetId(long targetId) {
        this.targetId = targetId;
        return this;
    }

    public PermissionTarget getTarget() {
        return target;
    }

    public Permission setTarget(PermissionTarget target) {
        this.target = target;
        return this;
    }

    public PermissionType getType() {
        return type;
    }

    public Permission setType(PermissionType type) {
        this.type = type;
        return this;
    }
}
